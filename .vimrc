" mandatory plugins
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim

"Set up plugins
call vundle#begin()

" Our plugins
Plugin 'gmarik/Vundle.vim'				" vundle
Plugin 'flazz/vim-colorschemes'			" nice colors!
Plugin 'scrooloose/nerdtree'			" nice file systm navigator
Plugin 'vim-airline/vim-airline'		" Air line at the bottom of vim
Plugin 'vim-airline/vim-airline-themes'	" Ait line themes
Plugin 'ctrlpvim/ctrlp.vim'				" Ctrl P fuzzy file finder
call vundle#end()

" enable 256 colors in the terminal
set term=screen-256color

" give us nice end of line, tab, etc chars
set list
set listchars=tab:▸\ ,eol:¬
set showbreak=↪

" Set color scheme
syntax enable
let g:solarized_termcolors=256
let g:solarized_termtrans=1
:colorscheme solarized 
let g:airline_theme='wombat'

" Show current position
set ruler

" Set height of the command line
set cmdheight=2

"Always show status line
set laststatus=2

"show line numbers
set relativenumber

" make backspace sane
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

"Turn on the wild menu
set wildmenu

" Searching
set ignorecase 	"Ignores case when searching
set smartcase 	" cares about case when there is case
set hlsearch 	"highlight search results
set incsearch 	" like modern browsers

"Show matching brackets
set showmatch
set mat=2 	"tenths of a second to blink when matching

"No annoying sounds on erros
set noerrorbells
set novisualbell
set t_vb=
set tm=500

" Extra margin on the left
set foldcolumn=1

" Remove backup stuff
set nobackup
set  nowb
set noswapfile

"Faster redrawing
set ttyfast

" Set magic on, for regex
set magic

"remap esc
inoremap ;; <esc>

" toggle cursor line 
nnoremap <leader>i :set cursorline!<cr>

" Tab control
set noexpandtab             " insert tabs rather than spaces for <Tab>
set smarttab                " tab respects 'tabstop', 'shiftwidth', and 'softtabstop'
set tabstop=4               " the visible width of tabs
set softtabstop=4           " edit as if the tabs are 4 characters wide
set shiftwidth=4            " number of spaces to use for indent and unindent
set shiftround              " round indent to a multiple of 'shiftwidth'
set completeopt+=longest
set autoindent
set smartindent

" Set where splits should occur
set splitbelow
set splitright

" Enable folding
set foldmethod=indent
set foldlevel=99

"Enable folding with the space bar
nnoremap <space> za

" Nerd Tree window size
let g:NERDTreeWinSize=2

" Disable the encryption key when attempting to save
cnoreabbrev <expr> X (getcmdtype() is# ':' && getcmdline() is# 'X') ? 'x' : 'X'

" Tabs windows 
nnoremap tn :tabnew<Space>
nnoremap tk :tabnext<CR>
nnoremap tj :tabprev<CR>
nnoremap tl :tablast<CR>
nnoremap th :tabfirst<CR>

function! Generate_HeaderFile()
	let curFileSize = getfsize(@%)
	if(curFileSize == 0) || (curFileSize == -1)
		let file_name = expand('%:t:r')
		let file_name_upper = toupper(file_name)
		let file_name_upper_h = file_name_upper . "_H"
		call append(0, "#ifndef " . file_name_upper_h)
		call append(1, "#define ". file_name_upper_h)
		call append(2, "")
		call append(3, "class " . file_name . "{")
		call append(4, "};")
		call append(5, "")
		call append(6, "#endif ")
	endif
endfunction

"Custom Command for .h files initial population
autocmd BufNewFile,BufReadPre *.h call Generate_HeaderFile()
