# Report mail count for your mailbox type

generate_segmentrc() {
	read -d '' rccontents  << EORC

EORC
	echo "${rccontents}"
}

run_segment() {
	local count
	local tmp_file="${TMUX_POWERLINE_DIR_TEMPORARY}/gmail_count.txt"

	local tmp_wgetrc="${TMUX_POWERLINE_DIR_TEMPORARY}/tmp_wgetrc.txt"
	local override_passget="false"	# When true a force reloaded will be done.

	# Create the cache file if it doesn't exist.
	if [ ! -f "$tmp_file" ]; then
		touch $tmp_file
		override_passget=true
	fi

	# Refresh mail count if the tempfile is older than $interval minutes.
	let interval=600
	last_update=$(stat -f "%m" ${tmp_file})
	if [ "$(( $(date +"%s") - ${last_update} ))" -gt "$interval" ] || [ "$override_passget" == true ]; then
		# Check for wget before proceeding.
		which wget 2>&1 > /dev/null
		if [ $? -ne 0 ]; then
			echo "This script requires wget." 1>&2
			return 1
		fi

    	# Hide password from command line (visible with e.g. ps(1)).
    	echo -e "user=eric.soppi@gmail.com\npassword=iamSHEFF6969$" > "$tmp_wgetrc"
		mail=$(wget -q -O - https://mail.google.com/a/gmail.com/feed/atom --config "$tmp_wgetrc" | grep -E -m 1 -o '<fullcount>(.*)</fullcount>' | sed -e 's,.*<fullcount>\([^<]*\)</fullcount>.*,\1,g')
		rm "$tmp_wgetrc"

		if [ "$mail" != "" ]; then
			echo $mail > $tmp_file
		else
			return 1
		fi
	fi

	count=$(cat $tmp_file)
	echo "$count"

	local exitcode="$?"
	if [ "$exitcode" -ne 0 ]; then
		return $exitcode
	fi

	if [[ -n "$count"  && "$count" -gt 0 ]]; then
		echo "✉ ${count}"
	fi

	return 0
}

